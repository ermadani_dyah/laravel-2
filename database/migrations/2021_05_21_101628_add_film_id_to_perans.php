<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmIdToPerans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perans', function (Blueprint $table) {
            $table->unsignedBigInteger('id_film');
            $table->foreign('id_film')->references('id')->on('films');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perans', function (Blueprint $table) {
            $table->dropeForeign(['id_film']);
            $table->dropColumn(['id_film']);
            
        });
    }
}
