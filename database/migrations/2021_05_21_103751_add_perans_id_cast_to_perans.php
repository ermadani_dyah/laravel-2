<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPeransIdCastToPerans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perans', function (Blueprint $table) {
            $table->unsignedBigInteger('id_casts_baru');
            $table->foreign('id_casts_baru')->references('id')->on('casts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perans', function (Blueprint $table) {
            $table->dropeForeign(['id_casts_baru']);
            $table->dropColumn(['id_casts_baru']);
        });
    }
}
