<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToKritiks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kritiks', function (Blueprint $table) {
            $table->unsignedBigInteger('film_id_baru');
            $table->unsignedBigInteger('id_user');
            $table->foreign('film_id_baru')->references('id')->on('films');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kritiks', function (Blueprint $table) {
            $table->dropeForeign(['id_user']);
            $table->dropColumn(['id_user']);
            $table->dropeForeign(['film_id_baru']);
            $table->dropColumn(['film_id_baru']);
        });
    }
}
