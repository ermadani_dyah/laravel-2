<!DOCTYPE html>
<html lang="en">
  <!--Head-->
  @include('include.head')
  <body class="hold-transition sidebar-mini">
    <div class="wrapper">
      <!-- Navbar -->
      @include('include.navbar')
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      @include('include.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        @yield('content')
      </div>
      <!-- /.content-wrapper -->
      <!--Footer-->
        @include('include.footer')
      <!--/.Footer-->

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <!-- script -->
     @include('include.script')
    <!-- ./script -->
  </body>
</html>
