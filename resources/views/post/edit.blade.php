@extends('welcome')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Edit Data</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="/cast">Cast</a>
                    </li>
                    <li class="breadcrumb-item active">Edit Cast</li>
                </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card-body">
                                <section class="content">
                                    <div class="card card-info">
                                        <div class="card-header">
                                            <h3 class="card-title" style="margin-top:5px;">
                                                <i class="far fa-list-alt"></i> Form Edit Cast
                                            </h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <br>
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/cast/{{$cast->id}}">
                                            @csrf
                                            @method('PUT')
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label for="nama" class="col-sm-3 col-form-label">Nama </label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="nama" class="form-control" id="nama" value="{{ old('nama',$cast->nama) }}">
                                                        @error('nama')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="umur" class="col-sm-3 col-form-label">Umur</label>
                                                    <div class="col-sm-7">
                                                        <input type="number" name="umur" class="form-control" id="umur" value="{{old('umur',$cast->umur)}}">
                                                        @error('umur')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="bio" class="col-sm-3 col-form-label">Bio</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="bio" class="form-control" id="bio" value="{{old('bio',$cast->bio)}}">
                                                        @error('bio')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-info float-right" name="submit">
                                                    <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Edit
                                                </button>
                                                <div class="text-left upgrade-btn">
                                                    <a href="{{route('cast.index')}}" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                        <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                        Kembali
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- /.card-footer -->
                                        </form>
                                    </div>
                                    <!-- /.card -->
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection