@extends('welcome')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Cast</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="index.php">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Cast</li>
                </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class="card-title" style="margin-top:5px;">
                                        <i class="mr-3 far fa-building"aria-hidden="true"></i> Daftar Cast
                                    </h3>
                                    <div class="card-tools">
                                        <a href="{{route('cast.create')}}" class="btn btn-sm btn-info float-right">
                                            <i class="fas fa-plus"></i> Tambah Daftar Cast
                                        </a>
                                    </div>
                                </div> 
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <form method="post" action="">
                                            <div class="row">
                                                <div class="col-md-4 bottom-10">
                                                    <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Nama Cabang">
                                                </div>
                                                <div class="col-md-5 bottom-10">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- .row -->
                                        </form>
                                    </div>
                                    <br>
                                    @if(session('sukses'))
                                        <div class="col-sm-12">
                                            <div class="alert alert-success">
                                                {{session('sukses')}}
                                            </div>
                                        </div>
                                    @endif
                                    <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                                <th width="5%">NO</th>
                                                <th width="15%">Nama</th>
                                                <th width="15%">Umur</th>
                                                <th width="15%">Bio</th>
                                                <th width="20%">
                                                    <center>Aksi</center>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>	
                                            @forelse($cast as $key=>$casts)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$casts->nama}}</td>
                                                    <td>{{$casts->umur}}</td>
                                                    <td>{{$casts->bio}}</td>
                                                    <td align="center">
                                                        <a href="{{route('cast.edit',['cast'=>$casts->id],'edit')}}" class="btn btn-xs btn-info">
                                                            <i class="fas fa-edit"></i> Edit
                                                        </a>
                                                        <form action="{{route('cast.destroy',['cast'=>$casts->id])}}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="submit" value="delete" class="btn btn-xs btn-warning">
                                                        </form> 
                                                        <a href="{{route('cast.show',['cast'=>$casts->id])}}" class="btn btn-xs bg-success" title="Detail">
                                                            <i class="fas fa-eye"></i>  Detail 
                                                        </a> 
                                                    </td>
                                                </tr>
                                                @empty
                                                <tr>
                                                    <td colspan="5" align="center">No Post</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection