@extends('welcome')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Detail Cast</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Cast</a>
                        </li>
                        <li class="breadcrumb-item active">Detail Cast</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card-body">
                                <section class="content">
                                    <div class="card card-info">
                                        <div class="card-header">
                                            <h3 class="card-title" style="margin-top:5px;">
                                                    <i class="far fa-eye"></i>Form Detail Cast
                                            </h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <br>
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <tbody>  
                                                    <tr>
                                                        <td colspan="2">
                                                            <i class="fas fa-user-circle"></i> 
                                                            <strong>Detail Cast</strong>
                                                        </td>
                                                    </tr>               
                                                    <tr>
                                                        <td width="20%">
                                                            <strong>Id Cast</strong>
                                                        </td>
                                                        <td width="80%">
                                                            {{$cast->id}}
                                                        </td>
                                                    </tr>                 
                                                    <tr>
                                                        <td width="20%">
                                                            <strong>Nama</strong>
                                                        </td>
                                                        <td width="80%">
                                                            {{$cast->nama}}
                                                        </td>
                                                    </tr>                 
                                                    <tr>
                                                        <td width="20%">
                                                            <strong>Umur</strong>
                                                        </td>
                                                        <td width="80%">
                                                            {{$cast->umur}}
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <td>
                                                            <strong>Bio<strong>
                                                        </td>
                                                        <td>
                                                            {{$cast->bio}}
                                                        </td>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="card-footer clearfix">
                                            <div class="text-right upgrade-btn">
                                                <a href="/cast" class="btn btn-sm btn-info d-none d-md-inline-block text-white">
                                                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                    Kembali
                                                </a>
                                            </div>
                                            &nbsp;
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection