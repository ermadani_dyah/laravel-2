<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/table', function () {
    return view('includes.content_table');
});
Route::get('/data-table', function () {
    return view('includes.content_data_table');
});
//MANUAL
// Route::get('/cast/create','CastController@create');
// Route::post('/cast','CastController@store');
// Route::get('/cast','CastController@index')->name('post.index');
// Route::get('/cast/{cast_id}','CastController@detail');
// Route::get('/cast/{id}/edit','CastController@edit');
// Route::put('/cast/{id}','CastController@update');
// Route::delete('/cast/{id}','CastController@delate');

//RESOURCE
// Route::resource('casting','CastingController');
Route::resource('cast','CastController');