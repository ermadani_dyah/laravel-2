<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\cast;
class CastController extends Controller
{
    public function create(){
        return view('post.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama'=>'required|unique:cast',
            'bio'=>'required',
            'umur'=>'required'
        ]);
        //BIASA
        // $query=DB::table('cast')->insert(
        //     [
        //         "nama"=>$request['nama'],
        //         "umur"=>$request['umur'],
        //         "bio"=>$request['bio']
        //     ]
        // );

        //ORM
        // $cast=new cast;
        // $cast->nama= $request['nama'];
        // $cast->umur=$request['umur'];
        // $cast->bio=$request['bio'];
        // $cast->save();

        //MASS ASSIGMENT
        $cast=cast::create([
            "nama"=>$request["nama"],
            "umur"=>$request["umur"],
            "bio"=>$request["bio"]
        ]);
        return redirect('/cast')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function index(){
        //query builder
        // $cast=DB::table('cast')->get();

        //ORM
        $cast=cast::all(); 
        return view('post.index',compact('cast'));
    }
    public function show($id){
        //query BUilder
        // $cast=DB::table('cast')->where('id',$id)->first();
        
        //ORM
        $cast=cast::find($id);
        return view('post.detail',compact('cast'));
    }
    public function edit($id){
        //QUERY BUILDER
        // $cast=DB::table('cast')->where('id',$id)->first();
        
        //ORM
        $cast=cast::find($id);
        return view('post.edit',compact('cast'));
    }
    public function update($id,Request $request){
        $request->validate([
            'nama'=>'required',
            'bio'=>'required',
            'umur'=>'required'
        ]);
        //QUERY BUILDER
        // $query=DB::table('cast')->where('id',$id)->update([
        //     "nama"=>$request['nama'],
        //     "umur"=>$request['umur'],
        //     "bio"=>$request['bio']
        // ]);
        
        //ORM
        $update=cast::where('id',$id)->update([
            "nama"=>$request['nama'],
            "umur"=>$request['umur'],
            "bio"=>$request['bio']
        ]);
        return redirect('/cast')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
    public function destroy($id){
        //QUERY BUILDR
        // $query=DB::table('cast')->where('id',$id)->delete();

        //ORM
        cast::destroy($id);
        return redirect('/cast')->with('sukses','Data Anda Berhasil Dihapus');
    }
}
